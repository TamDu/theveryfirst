﻿using UnityEngine;
using System.Collections;

public class Delete : MonoBehaviour {

	public GameObject child;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerExit(Collider collidedObject)
	{
		if ( collidedObject.gameObject.tag == "Player" )
		{
			child.GetComponent<Renderer>().enabled = true;
			child.GetComponent<Collider>().enabled = true;
		}
	}
}
