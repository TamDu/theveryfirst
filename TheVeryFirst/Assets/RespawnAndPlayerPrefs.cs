﻿using UnityEngine;
using System.Collections;

public enum PLAYER_STATS
{
	CheckPointOne,
	CheckPointTwo,
	CheckPointThree,
	Start
}

public class RespawnAndPlayerPrefs : MonoBehaviour 
{
	public PLAYER_STATS enum_playerStats;
	
	public Transform[] Trans_respawnPosition;
	public GameObject GO_playerLaserWeapon;
	public GameObject GO_particleEffects;
	
	public float f_fuelThreshold;
	public float f_fuelCap;
	public float f_currentFuel;
	
	public float f_startFuelThreshold;
	public float f_startFuelCap;
	
	private void Start()
	{
		checkElement();
		updateStats();
		transform.position = Trans_respawnPosition[PlayerPrefs.GetInt( "RespawnElement" )].position;
	}
	
	private void checkElement()
	{
		switch ( PlayerPrefs.GetInt( "RespawnElement" ) )
		{
			case 1:
				enum_playerStats = PLAYER_STATS.CheckPointOne;
				break;
			case 2:
				enum_playerStats = PLAYER_STATS.CheckPointTwo;
				break;
			case 3:
				enum_playerStats = PLAYER_STATS.CheckPointThree;
				break;
			default:
				enum_playerStats = PLAYER_STATS.Start;
				break;
		}
	}
	
	private void updateStats()
	{
		if ( enum_playerStats == PLAYER_STATS.CheckPointOne )
		{
			activatePlayerBoost( true );
			activatePlayerLaser( false );
		}
		
		if ( enum_playerStats == PLAYER_STATS.CheckPointTwo )
		{
			activatePlayerBoost( true );
			activatePlayerLaser( false );
		}
		
		if ( enum_playerStats == PLAYER_STATS.CheckPointThree )
		{
			activatePlayerBoost( true);
			activatePlayerLaser( true );
		}
		
		if ( enum_playerStats == PLAYER_STATS.Start )
		{
			activatePlayerBoost( false );
			activatePlayerLaser( false );
		}
	}
	
	private void activatePlayerBoost( bool yesOrNo )
	{
		PlayerFuel playerFuelComponent =  GetComponent<PlayerFuel>();
		playerFuelComponent.b_havedObtainedBoost = yesOrNo;
		playerFuelComponent.script_playerBoost.enabled = yesOrNo;
		GO_particleEffects.SetActive( yesOrNo );
		if ( yesOrNo )
		{
			playerFuelComponent.fuel.f_useableFuelThreshold = f_fuelThreshold;
			playerFuelComponent.fuel.f_fuelCapacity = f_fuelCap;
			playerFuelComponent.fuel.f_currentFuel = f_currentFuel;
		}
		else
		{
			playerFuelComponent.fuel.f_useableFuelThreshold = f_startFuelThreshold;
			playerFuelComponent.fuel.f_fuelCapacity = f_startFuelCap;
		}
	}
	
	private void activatePlayerLaser( bool yesOrNo )
	{
		GO_playerLaserWeapon.SetActive( yesOrNo );
	}
}
