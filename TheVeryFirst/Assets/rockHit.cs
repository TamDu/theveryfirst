﻿using UnityEngine;
using System.Collections;

public class rockHit : MonoBehaviour 
{
	public GameObject GO_disableObject;
	public GameObject Go_enableObject;
	
	private void OnCollisionEnter( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == "rock" )
		{
			GO_disableObject.SetActive( false );
			gameObject.SetActive( false );
			Go_enableObject.SetActive( true );
		}
	}
}
