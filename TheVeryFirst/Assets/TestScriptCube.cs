﻿using UnityEngine;
using System.Collections;

public class TestScriptCube : MonoBehaviour {

	// Use this for initialization
	public Transform[] arr_bossCP;
	public int i_currentCP;
	public float f_ms;
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		fourConnerMove();
	}
	void fourConnerMove()
	{
		
		GetComponent<Renderer>().material.color = Color.green;
		if(transform.position == arr_bossCP[i_currentCP].position )
		{
			i_currentCP = Random.Range(0, arr_bossCP.Length);
		}
		transform.position = Vector3.MoveTowards(transform.position, arr_bossCP[i_currentCP].position, f_ms * Time.deltaTime); 
	}
}
