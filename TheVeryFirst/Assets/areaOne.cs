﻿using UnityEngine;
using System.Collections;

public class areaOne : MonoBehaviour 
{
	public GameObject Go_turretLaserGun;
	
	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == "Player" )
		{
			Go_turretLaserGun.GetComponent<turretGunFire>().b_turretActivated = true;
		}
	}
}
