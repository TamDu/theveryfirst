﻿using UnityEngine;
using System.Collections;

public class bossTrigger : MonoBehaviour 
{
	public GameObject GO_closePlatform;
	public GameObject GO_monetBoss;
	public string s_playerTag;
	
	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			turnOnBoss();
		}
	}
	
	public void turnOnBoss()
	{
		GO_closePlatform.SetActive( true );
		GO_monetBoss.GetComponent<moNetOneBoss>().b_bossActivate = true;
	}
}
