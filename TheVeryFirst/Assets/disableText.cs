﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class disableText : MonoBehaviour 
{
	public string s_playerTag;
	public Text txt_tutorial;
	
	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			txt_tutorial.gameObject.SetActive( false );
		}
	}
}
