﻿using UnityEngine;
using System.Collections;

public class laserDetectionBeam : MonoBehaviour 
{
	public string s_playerTag;
	public GameObject GO_deathBeam;
	
	public float f_obstcleActivationTimer;
	private float f_obstcleActivationCurrentTime; 
	public float f_obstcleTimer;
	private float f_currentObstcleTime;
	
	private bool b_beamOn = false;
	private bool b_recentPlayerDetection = false;

	private void Start()
	{
		gameObject.GetComponent<Renderer>().material.color = Color.magenta;
	}
	
	private void Update()
	{
		activateBeam();
	}

	public void activateBeam()
	{
		if ( b_beamOn )
		{
			f_currentObstcleTime -= Time.deltaTime;
			if ( f_currentObstcleTime > 0 )
			{
				beamStatus( true );
			}
			else
			{
				b_beamOn = false;
				gameObject.GetComponent<Renderer>().material.color = Color.magenta;
			}
		}
		else
		{	
			beamStatus( false );
		}
		
		f_obstcleActivationCurrentTime -= Time.deltaTime;
		if ( f_obstcleActivationCurrentTime <= 0
			&& b_recentPlayerDetection
			)
		{
			b_beamOn = true;
			f_currentObstcleTime = f_obstcleTimer;
			b_recentPlayerDetection = false;
		}
	}
	
	private void beamStatus( bool yesOrNo )
	{
		GO_deathBeam.GetComponent<Renderer>().enabled = yesOrNo;
		GO_deathBeam.GetComponent<Collider>().enabled = yesOrNo;
	}
	
	private void playerTriggerEvent()
	{
		f_obstcleActivationCurrentTime = f_obstcleActivationTimer;
		b_recentPlayerDetection = true;
		
		gameObject.GetComponent<Renderer>().material.color = Color.red;
	}

	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			playerTriggerEvent();
		}
	}
}
