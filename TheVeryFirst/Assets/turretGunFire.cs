﻿using UnityEngine;
using System.Collections;

public class turretGunFire : MonoBehaviour 
{
	public bool b_turretActivated;
	public int i_shotsPerFire;
	public float f_fireRate;
	public GameObject GO_laserSpawn;
	public GameObject Go_laserShot;
	public float f_timeTillNextShot;
	
	private void Update()
	{
		turretFire();
	}
	
	private void turretFire()
	{
		if ( b_turretActivated )
		{
			f_timeTillNextShot -= Time.deltaTime;
			
			if ( f_timeTillNextShot <= 0 )
			{
				f_timeTillNextShot = f_fireRate;
				turretShooting();
			}
		}
	}
	
	private void turretShooting()
	{
		Instantiate( Go_laserShot, GO_laserSpawn.transform.position, GO_laserSpawn.transform.rotation );
		Instantiate( Go_laserShot, GO_laserSpawn.transform.position, GO_laserSpawn.transform.rotation );
	}
}
