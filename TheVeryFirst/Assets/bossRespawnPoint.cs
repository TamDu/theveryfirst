﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class bossRespawnPoint : MonoBehaviour
{
	public string s_playerTag;
	public int i_respawnElement;
	public float f_txtFadeTimer;
	public float f_currentTxtFadeTime;
	public Text txt_SavedSpawnPoint;
	
	private void Update()
	{
		saveText();
	}
	
	private void OnCollisionEnter( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			print ( "yes" );
			print( PlayerPrefs.GetInt( "RespawnElement" ) );
			PlayerPrefs.SetInt( "RespawnElement", i_respawnElement );
			f_currentTxtFadeTime = f_txtFadeTimer;
		}
	}
	
	private void saveText()
	{
		if ( f_currentTxtFadeTime > 0 )
		{
			f_currentTxtFadeTime -= Time.deltaTime;
			txt_SavedSpawnPoint.gameObject.SetActive( true );
		}
		else
		{
			txt_SavedSpawnPoint.gameObject.SetActive( false );
		}
	}
}
