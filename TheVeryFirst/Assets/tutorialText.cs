﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class tutorialText : MonoBehaviour 
{
	public Text tutorielText;
	public string s_playerTag;
	public float f_fadeTime;
	private float f_currentFadeTime;
	
	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			tutorielText.gameObject.SetActive( true );
		}
	}
}
