﻿using UnityEngine;
using System.Collections;

public class stickyPlate : MonoBehaviour 
{
	public GameObject GO_player;
	public Vector3 vec_playerPos;
	public Vector3 vec_stickyPlateMoveDirection;
	public float f_stickPlateSpeed;
	public float f_moveDuration;
	public float f_landingThresholdMax;
	public float f_landingThresholdMin;
	public Vector3 vec_landingDirection;
	public float f_movePlayerToDeathTimer;
	private float f_moveTime;
	private bool b_hitPlayer = false;
	private bool b_correctDirection = false;
	
	private void Start()
	{
		f_moveTime = f_moveDuration;
	}
	
	private void Update()
	{
		hitPlayer();
	}

	private void OnCollisionEnter( Collision collidedObject )
	{
		vec_playerPos = collidedObject.gameObject.transform.position;
		vec_landingDirection = collidedObject.contacts[0].normal;
		stickingToPlate();
		if ( collidedObject.gameObject.tag == "Player" 
			&& b_correctDirection
			)
		{
			PlayerMovement playerMovementComponent = collidedObject.gameObject.GetComponent<PlayerMovement>();
			PlayerBoost playerBoostComponent = collidedObject.gameObject.GetComponent<PlayerBoost>();
			Restrictions playerRestrictionsComponent = collidedObject.gameObject.GetComponent<Restrictions>();
			PlayerFuel playerFuelComponent = collidedObject.gameObject.GetComponent<PlayerFuel>();
			if ( playerMovementComponent != null 
				|| playerBoostComponent != null
				)
			{
				b_hitPlayer = true;
				playerMovementComponent.enabled = false;
				playerBoostComponent.enabled = false;
				playerFuelComponent.enabled = false;
				playerRestrictionsComponent.f_Gravity = 0;
				playerRestrictionsComponent.f_MaxVelocity = 0;
			}
		}
	}
	
	private void hitPlayer()
	{
		if ( b_hitPlayer )
		{
			f_moveTime -= Time.deltaTime;
			
			if ( f_moveTime > 0 )
			{
				gameObject.transform.Translate( vec_stickyPlateMoveDirection * f_stickPlateSpeed * Time.deltaTime );
				GO_player.transform.Translate( vec_stickyPlateMoveDirection * f_stickPlateSpeed * Time.deltaTime );
				vec_playerPos = GO_player.transform.position;
				GO_player.transform.position = vec_playerPos;
			}
			else
			{
				f_movePlayerToDeathTimer -= Time.deltaTime;
			}
			
			if ( f_movePlayerToDeathTimer <= 0 )
			{
				GO_player.transform.Translate( vec_stickyPlateMoveDirection * f_stickPlateSpeed * Time.deltaTime );
			}
		}
	}
	
	private void stickingToPlate()
	{
		if ( vec_landingDirection.y > f_landingThresholdMin
			&& vec_landingDirection.y < f_landingThresholdMax
			)
		{
			b_correctDirection = true;
		}
		else
		{
			b_correctDirection = false;
		}
	}
}
