﻿using UnityEngine;
using System.Collections;

public class BoostPickUp : MonoBehaviour 
{
	public string s_playerTag;
	public float f_thresholdIncrease;
	public float f_maxFuelCapIncrease;
	public GameObject GO_boostParticleEffects;
	
	private void OnTriggerEnter( Collider collidedObject )	
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			PlayerFuel playerFuelComponent = collidedObject.gameObject.GetComponent<PlayerFuel>();
			if ( playerFuelComponent != null )
			{
				playerFuelComponent.b_havedObtainedBoost = true;
				playerFuelComponent.script_playerBoost.enabled = true;
				playerFuelComponent.fuel.f_fuelCapacity = f_maxFuelCapIncrease;
				playerFuelComponent.fuel.f_useableFuelThreshold = f_thresholdIncrease;
				GO_boostParticleEffects.SetActive( true );
				Destroy( gameObject );
			}
		}
	}
}
