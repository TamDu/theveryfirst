﻿using UnityEngine;
using System.Collections;

public class ColourChange : MonoBehaviour 
{
	public bool b_playerDetected;
	// Use this for initialization
	private void Update() 
	{
		changeColour();
	}
	
	private void changeColour()
	{
		if ( b_playerDetected )
		{
			gameObject.GetComponent<Renderer>().material.color = Color.red;	
		}
		else
		{
			gameObject.GetComponent<Renderer>().material.color = Color.magenta;
		}
	}
}
