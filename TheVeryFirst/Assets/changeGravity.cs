﻿using UnityEngine;
using System.Collections;

public class changeGravity : MonoBehaviour 
{
	public string s_playerTag;
	public float f_gravityChange;
	
	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			Restrictions restrictionComponent = collidedObject.GetComponent<Restrictions>();
			if ( restrictionComponent != null )
			{
				restrictionComponent.f_Gravity = f_gravityChange;
			}
		}
	}
}
