﻿using UnityEngine;
using System.Collections;

public class fanSuckUp : MonoBehaviour 
{
	public string s_PlayerTag;
	public float f_changedGravity;
	public bool b_isFanAlive = true;
	public float f_fanDeathTimer;
	public GameObject GO_fanControl;
	private float f_initialGravity;
	
	private void Update()
	{
		fanDying();
	}
	
	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_PlayerTag 
			&& b_isFanAlive
			)
		{
			Restrictions restrictionsComponent = collidedObject.GetComponent<Restrictions>();
			if ( restrictionsComponent != null )
			{
				f_initialGravity = restrictionsComponent.f_Gravity;
				restrictionsComponent.f_Gravity = f_changedGravity;
			}
		}
	}
	
	private void OnTriggerExit( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_PlayerTag )
		{
			Restrictions restrictionComponent = collidedObject.gameObject.GetComponent<Restrictions>();
			if ( restrictionComponent != null )
			{
				restrictionComponent.f_Gravity = f_initialGravity;
			}
		}
	}
	
	private void fanDying()
	{
		if ( GO_fanControl == null )
		{
			b_isFanAlive = false;
			f_fanDeathTimer -= Time.deltaTime;
			
			if ( f_fanDeathTimer <= 0 )
			{
				Destroy( gameObject );
			}
		}
	}
}
