﻿using UnityEngine;
using System.Collections;

public class rotatePickUp : MonoBehaviour 
{
	public string s_playerTag;
	public GameObject GO_playerGun;
	public float f_angleX;
	public float f_angleY;
	public float f_angleZ;
	
	private void Update()
	{
		rotate();
	}
	
	private void rotate()
	{
		transform.Rotate( f_angleX, f_angleY, f_angleZ, Space.World );
	}
	
	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			GO_playerGun.SetActive( true );
			Destroy( gameObject );
		}
	}
}
