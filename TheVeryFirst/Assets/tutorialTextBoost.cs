﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class tutorialTextBoost : MonoBehaviour 
{
	public Text txt_boostText;
	public string s_playerTag;
	
	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			setBoostTxtStatus( true );
		}
	}
	
	private void OnTriggerExit( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			setBoostTxtStatus( false );
		}
	}
	
	private void setBoostTxtStatus( bool yesOrNo )
	{
		txt_boostText.gameObject.SetActive( yesOrNo );
	}
}
