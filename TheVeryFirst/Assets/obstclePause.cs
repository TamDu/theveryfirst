﻿using UnityEngine;
using System.Collections;

public class obstclePause : MonoBehaviour 
{
	public movingShipScript movingshipScript;
	public Transform pauseSpot;
	public Transform refreshSpot;
	public float f_pauseDuration;
	private float f_currentDuration;
	
	private void Update()
	{
		pausePatrol();
	}
	
	private void pausePatrol()
	{
		if ( pauseSpot.position == gameObject.transform.position )
		{	
			if ( f_currentDuration > 0 )
			{
				f_currentDuration -= Time.deltaTime;
				movingshipScript.enabled = false;
			}
			else
			{
				movingshipScript.enabled = true;
			}
		}
		
		if ( refreshSpot.position == gameObject.transform.position )
		{
			f_currentDuration = f_pauseDuration;
		}
	}
}
