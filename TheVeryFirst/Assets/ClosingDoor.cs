﻿using UnityEngine;
using System.Collections;

public class ClosingDoor : MonoBehaviour 
{
	public Renderer detectionLaser;
	public Transform door;
	public Transform doorMoveTo;
	public float f_speed;
	private Vector3 vec_initalPos;
	private bool b_playerHasEntered = false;
	
	private void Start()
	{
		detectionLaser.material.color = Color.gray;
		vec_initalPos = door.position;
	}
	
	private void Update()
	{
		positionOfDoor();
	}
	
	private void positionOfDoor()
	{
		if ( b_playerHasEntered )
		{
			moveDoor( Color.green, doorMoveTo.position );
		}
		else
		{
			moveDoor( Color.gray, vec_initalPos );
		}
	}
	
	private void OnTriggerEnter( Collider collidedObject)
	{
		b_playerHasEntered = true;
	}
	
	private void OnTriggerExit( Collider collidedObject )
	{
		b_playerHasEntered = false;
	}
	
	private void moveDoor( Color colourOfDoor, Vector3 moveTo )
	{
		detectionLaser.material.color = colourOfDoor;
		door.position = Vector3.MoveTowards( door.position, moveTo, Time.deltaTime * f_speed );
	}
}
