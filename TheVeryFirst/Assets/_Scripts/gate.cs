using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Gate
{
	public GameObject GO_gate;
	public bool b_closeGateWhenTriggered;
}

public class gate : MonoBehaviour 
{	
	public string s_playerTag;
	public Gate[] arr_gate;
	
	private void OnCollisionEnter( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			for ( int i_ceT = 0; i_ceT < arr_gate.Length; i_ceT ++ )
			{
				changingGate( i_ceT, arr_gate[i_ceT].b_closeGateWhenTriggered );
			}
		}
	}
	
	private void OnTriggerEnter( Collider collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_playerTag )
		{
			for ( int i_ceT = 0; i_ceT < arr_gate.Length; i_ceT ++ )
			{
				changingGate( i_ceT, arr_gate[i_ceT].b_closeGateWhenTriggered );
			}
		}
	}
	
	private void changingGate( int i_ce, bool open )
	{
		arr_gate[i_ce].GO_gate.SetActive( open );
	}
}
