using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public struct BoostControls
{
	public string s_nameOfBoost;
	public Vector3 vec_directionOfBoost;
	public KeyCode boostdirectionKey;
	public KeyCode boostActivationKey;
	public GameObject GO_boostParticleEffects;
}

[System.Serializable]
public struct BoostSpeed
{
	public string s_nameOfBoostSpeed;
	public float f_boost;
	public KeyCode changeBoostSpeedKey;
}

[System.Serializable]
public class BoostCoolDown
{
	public float f_boostDurationTime;
	public float f_boostCoolDown;
	
	[HideInInspector]
	public float f_boostCurrDur;
	[HideInInspector]
	public float f_boostCurrCD;
}


public class PlayerBoost : MonoBehaviour 
{
	public Slider sl_boostBar;
	public BoostControls[] boostControls;
	public BoostSpeed[] boostSpeed;
	public BoostCoolDown boostCoolDown;
	public bool b_boostTimer;
	
	private bool b_overHeated;
	private float f_Pulse;
	public float f_ShiftPulse;
	public PlayerHealthAndDamage outOfFuel;
	
	private void Start () 
	{
		errorCheck();
		boostCoolDown.f_boostCurrDur = boostCoolDown.f_boostDurationTime;
		boostCoolDown.f_boostCurrCD = 0;
		f_Pulse = boostSpeed[0].f_boost;
	}
	
	private void FixedUpdate()
	{
		checkBoostKey();
		checkingBoost();
		fuelBar();
	}
	
	private void boost( Vector3 direction, float ms )
	{
		GetComponent<Rigidbody>().AddForce( direction * ms, ForceMode.Impulse );
	}
	
	private void boostCtrls(int i_ce)
	{
		if ( Input.GetKey( boostControls[i_ce].boostActivationKey ) 
			&& Input.GetKey( boostControls[i_ce].boostdirectionKey ) 
			)
		{
			usingBoost ();
			particleEffectStatus( i_ce, true );
			if ( b_boostTimer )
			{
				boost( boostControls[i_ce].vec_directionOfBoost, f_ShiftPulse );
			}
			else
			{
				boost( boostControls[i_ce].vec_directionOfBoost, f_Pulse );
			}
		}
		else
		{
			particleEffectStatus( i_ce, false );
			f_ShiftPulse = 0;
		}
	}
	
	private void checkingBoost()
	{
		for ( int i = 0; i < boostControls.Length; i++ )
		{
			boostCtrls( i );
		}
	}
	
	
	private void checkBoostKey()
	{
		for ( int i_ce = 0; i_ce < boostSpeed.Length; i_ce++ )
		{
			if ( Input.GetKeyDown(boostSpeed[i_ce].changeBoostSpeedKey) )
			{
				f_Pulse = boostSpeed[i_ce].f_boost;
			}
		}
	}
	
	
	//when reading current means the current cooldown n.o or current usage left 
	private void usingBoost()
	{
		if ( b_boostTimer )
		{
			//shift pulse current movement speed, pulse stored value after press pulse keys
			if ( !b_overHeated && boostCoolDown.f_boostCurrDur > 0 )
			{
				f_ShiftPulse = f_Pulse;
				boostCoolDown.f_boostCurrDur -= Time.deltaTime;
			}
			else if ( boostCoolDown.f_boostCurrDur <= 0 && !b_overHeated )
			{
				b_overHeated = true;
				boostCoolDown.f_boostCurrCD = boostCoolDown.f_boostCoolDown;
			}
		}
	}
	
	private void usingTimer()
	{
		if ( b_boostTimer )
		{
			overHeated ();
			clampBoostTimer();
		}
	}
	
	//when you go over boost usage
	private void overHeated()
	{
		if ( b_overHeated )
		{
			f_ShiftPulse = 0;
			if ( boostCoolDown.f_boostCurrCD > 0 )
			{
				boostCoolDown.f_boostCurrCD -= Time.deltaTime;
			}
			else if ( boostCoolDown.f_boostCurrCD <= 0 )
			{ 
				b_overHeated = false;
				boostCoolDown.f_boostCurrDur = boostCoolDown.f_boostDurationTime;
			}
		}
	}
	
	private void clampBoostTimer()
	{
		boostCoolDown.f_boostCurrCD = 
		Mathf.Clamp 
			(	boostCoolDown.f_boostCurrCD
			  , 0.0f
			  , boostCoolDown.f_boostCoolDown
			);
		boostCoolDown.f_boostCurrDur = 
		Mathf.Clamp 
			(	boostCoolDown.f_boostCurrDur
			  , 0.0f
			  , boostCoolDown.f_boostDurationTime
			);
	}
	
	
	public void particleEffectStatus( int i_ce, bool yesOrNo )
	{
		if ( !b_overHeated )
		{
			//s	GetComponent<PlayerMovement>().b_turnOnParticles = yesOrNo;
			boostControls[i_ce].GO_boostParticleEffects.SetActive( yesOrNo );	
		}
		else
		{
			//	GetComponent<PlayerMovement>().b_turnOnParticles = true;
			boostControls[i_ce].GO_boostParticleEffects.SetActive( false );
		}
	}
	
	private void fuelBar()
	{
		sl_boostBar.value = boostCoolDown.f_boostCurrDur;
	}
	
	
	private void errorCheck()
	{
		for ( int i_ce = 0; i_ce < boostControls.Length; i_ce++ )
		{	
			if ( boostControls[i_ce].vec_directionOfBoost == new Vector3(0,0,0) )
			{
				framingDebugbbCTRL( "There is no set direction:", i_ce, boostControls );
			}
			
			if ( boostControls[i_ce].boostdirectionKey == KeyCode.None 
				|| boostControls[i_ce].boostActivationKey == KeyCode.None 
				)
			{
				framingDebugbbCTRL( "There is no key set: ", i_ce, boostControls );
			}
		}
		
		for ( int i_ce = 0; i_ce < boostSpeed.Length; i_ce++ )
		{
			if ( boostSpeed[i_ce].f_boost <= 0 )
			{
				framingDebugbstS( "boost is lower than 0 in: ", i_ce, boostSpeed );
			}
			
			if ( boostSpeed[i_ce].changeBoostSpeedKey == KeyCode.None )
			{
				framingDebugbstS( "No key is set in: ", i_ce, boostSpeed );
			}
		}
		

		if ( boostCoolDown.f_boostDurationTime <= 0 )
		{
			Debug.LogWarning( "boost Duration is less or equal to 0" );
		}
		
		if ( boostCoolDown.f_boostCoolDown <= 0 )
		{
			Debug.LogWarning( "boost boos Cool Down is less or equal to 0" );
		}
		
	}
	
	
	//error check
	private void framingDebugbbCTRL( string text, int i_currentElement, BoostControls[] bCTRL )				  
	{
		Debug.LogError
			(	text
			 + bCTRL[i_currentElement].s_nameOfBoost
			 + "(element: " 
			 + i_currentElement 
			 + ")" 
			 );
	}
	
	private void framingDebugbstS( string text, int i_currentElement, BoostSpeed[] bstS )				  
	{
		Debug.LogError
			(	text
			 + bstS[i_currentElement].s_nameOfBoostSpeed
			 + "(element: " 
			 + i_currentElement 
			 + ")" 
			 );
	}
}