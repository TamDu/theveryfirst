﻿using UnityEngine;
using System.Collections;

// classes holding movement variables
[System.Serializable]
public struct ThePlayerMovement
{
	public string s_nameOfMovement;
	public float f_playerMovementSpeed;
	public Vector3 vec_direction;
	public KeyCode playerMovementKey;
	
	public GameObject GO_gameEngineParticleEffects;
	[HideInInspector]
	public bool b_isKeyHeld;
}

[RequireComponent ( typeof( Rigidbody ))]
public class PlayerMovement : MonoBehaviour 
{
	public bool b_turnOnParticles;
	public ThePlayerMovement[] playerMovement;
	private int i_keyHeldElement;
	private int i_secondKeyHeldElement;
	//public PlayerHealthAndDeathAndFuelCells outOfFuel;
	void Start () 
	{
		errorCheck();
	}
	
	private void FixedUpdate () 
	{
		checkMovementKey();
	}
	
	public bool checkKeyHeld()
	{
	
		if ( playerMovement[0].b_isKeyHeld 
		    || playerMovement[1].b_isKeyHeld
		    || playerMovement[2].b_isKeyHeld
		    || playerMovement[3].b_isKeyHeld 
			)
		{
			return true;
		}
		return false;
	}
	
	private void checkMovementKey()
	{
		for ( int i = 0; i < playerMovement.Length; i++ )
		{
			if ( KeyHeld( playerMovement[i].playerMovementKey ) )
			{
				movePlayer
					(	playerMovement[i].vec_direction 
				 	  , playerMovement[i].f_playerMovementSpeed 
					);
			if ( b_turnOnParticles )
				{
					playerMovement[i].GO_gameEngineParticleEffects.SetActive( true );
				}
				playerMovement[i].b_isKeyHeld = true;
			}
			else
			{
				playerMovement[i].b_isKeyHeld = false;
				playerMovement[i].GO_gameEngineParticleEffects.SetActive( false );
			}
		}
	}
	
	private bool KeyHeld( KeyCode key )
	{
		if ( Input.GetKey( key ) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private void movePlayer( Vector3 direction, float moveSpeed )
	{
		GetComponent<Rigidbody>().AddForce( direction * moveSpeed );
	}
	
	//checks errors
	private void errorCheck()
	{
		for ( int i =0; i < playerMovement.Length; i++ )
		{
			if ( playerMovement[i].vec_direction == new Vector3( 0, 0, 0) )
			{ 
				framingDebug( "No set direction in: ", i );
			}
			
			if (playerMovement[i].f_playerMovementSpeed <= 0)
			{
				framingDebug( "PlayerMovementSpeed is less than or equal to zero: ", i );
			}
			
			if ( playerMovement[i].playerMovementKey == KeyCode.None )
			{
				framingDebug( "No button is set in the playerMovementKey:", i );
			}
		}
	}
	
	private void framingDebug( string text, int i_currentElement )
	{
		Debug.LogError
			(	text
			  + playerMovement[i_currentElement].s_nameOfMovement 
			  + "(element: " 
			  + i_currentElement 
			  + ")"
			);
	}
}
