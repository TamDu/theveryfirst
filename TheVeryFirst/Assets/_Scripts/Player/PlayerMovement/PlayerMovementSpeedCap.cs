﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MaxAndMinVelocityAndControl
{
	public float f_MaxVelocity;
	public KeyCode maxVelocityKey;
}

public class PlayerMovementSpeedCap : MonoBehaviour {

	public float f_DefaultMaxVelcoity;
	public MaxAndMinVelocityAndControl[] maxAndMinVelocityAndControl;
	private int i_currentElement;
	private float f_maxVel;
	private float f_minVel;
	
	// Use this for initialization
	void Start () 
	{
		f_maxVel = f_DefaultMaxVelcoity;
	}
	
	// Update is called once per frame
	void Update () 
	{
		checkKeyPress();
	}
	
	
	void velocityKeys()
	{
		if ( Input.GetKeyDown( maxAndMinVelocityAndControl[i_currentElement].maxVelocityKey ) )
		{
			f_maxVel = maxAndMinVelocityAndControl[i_currentElement].f_MaxVelocity;
		}
	}
	
	void checkKeyPress()
	{
		for ( i_currentElement = 0; i_currentElement < maxAndMinVelocityAndControl.Length; i_currentElement++ )
		{
			velocityKeys();
		}
	}	
}
