﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class Fuel
{
	public float f_currentFuel;
	public float f_fuelCapacity;
	public float f_fuelRegeneration;
	public float f_fuelRegenerationOnPlatform;
	public float f_fuelUsage;
	public float f_useableFuelThreshold;
	
	public float f_currF()
	{
		return f_currentFuel;
	}
	public float f_fCap()
	{
		return f_fuelCapacity;
	}
	public float f_fRegen()
	{
		return f_fuelRegeneration;
	}
	public float f_fUse()
	{
		return f_fuelUsage;
	}
}

//moNet
public class PlayerFuel : MonoBehaviour 
{
	public string s_platformTag;
	public Slider sl_fuelBar;
	public float f_landingThreshold;
	public Fuel fuel;
	public PlayerMovement script_playerMovement;
	public PlayerBoost script_playerBoost;
	
	private bool b_onPlatform;
	private bool b_outOfFuel = false;
	private Vector3 vec_landingDirection;
	public bool b_havedObtainedBoost;
	
	// Update is called once per frame
	private void Update () 
	{
		checkFuel();
		fuelBar();
		
		if ( b_onPlatform )
		{
			GetComponent<PlayerMovement>().b_turnOnParticles = false;
		}
		else
		{
			GetComponent<PlayerMovement>().b_turnOnParticles = true;
		}
	}
	
	private void checkFuel()
	{
		Mathf.Clamp(fuel.f_currentFuel, 0.0f ,fuel.f_fCap());
		useableFuelCheck();
		isPlayerOutOfFuel();
		if ( !b_outOfFuel )
		{
			checkPlayerMovement();
		}
	}
	
	//fuel useage calling bools from other scripts to determine if player is moving
	//fcap = fuel capacity 
	private void checkPlayerMovement()
	{
		if ( script_playerMovement.checkKeyHeld()
			&& !b_onPlatform
			)
		{
			fuel.f_currentFuel -= fuel.f_fuelUsage * Time.deltaTime;
		}
		else	
		{
			if ( fuel.f_currentFuel < fuel.f_fCap() )
			{
				fuel.f_currentFuel += fuel.f_fRegen() * Time.deltaTime;	
			}
		}
	}
	
	//cannot explain why i'm using two bools, require a flux capacitor for answers
	// CurrF = currentfuel
	private void useableFuelCheck()
	{
		if ( fuel.f_currF() >= fuel.f_useableFuelThreshold 
			&& b_outOfFuel )
		{
			changeOutOfFuelStatus( false );
		}
	}
	
	private void isPlayerOutOfFuel()
	{
		if ( fuel.f_currF() <= 0 )
		{
			changeOutOfFuelStatus( true );
		}
		
		if ( b_outOfFuel )
		{
			if ( b_onPlatform )
			{
				fuel.f_currentFuel += ( fuel.f_fuelRegenerationOnPlatform * Time.deltaTime );
			}
			else
			{
				fuel.f_currentFuel += ( fuel.f_fRegen() * Time.deltaTime );
			}
		}
	}
	
	public bool changeOutOfFuelStatus( bool outOfFuel )
	{
		if ( outOfFuel )
		{
			script_playerMovement.enabled = false;
			for ( int i_ceT = 0; i_ceT < script_playerMovement.playerMovement.Length; i_ceT ++ )
			{
				script_playerMovement.playerMovement[i_ceT].GO_gameEngineParticleEffects.SetActive( false );
			}
			
			script_playerBoost.enabled = false;
			for ( int i_ceT = 0; i_ceT < script_playerBoost.boostControls.Length; i_ceT ++ )
			{
				script_playerBoost.particleEffectStatus( i_ceT, false );
			}
			return b_outOfFuel = true;
		}
		else
		{
			script_playerMovement.enabled = true;
			if ( b_havedObtainedBoost )
			{
				script_playerBoost.enabled = true;
			}
			return b_outOfFuel = false;
		}
	}
	
	private void OnCollisionEnter( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_platformTag)
		{
			vec_landingDirection = collidedObject.contacts[0].normal;
			
			checkJumpThreshold();
		}
	}
	
	private void OnCollisionStay( Collision collision )
	{
		if ( collision.gameObject.tag == s_platformTag )
		{
			vec_landingDirection = collision.contacts[1].normal;
			checkJumpThreshold();
		}
	}
	
	private void OnCollisionExit( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == s_platformTag )
		{
			b_onPlatform = false;
		}
	}
	
	private void checkJumpThreshold()
	{
		if ( vec_landingDirection.y > f_landingThreshold)
		{
			b_onPlatform = true;
		}
		else
		{
			b_onPlatform = false;
		}
	}
	
	private void fuelBar()
	{
		sl_fuelBar.value = fuel.f_currentFuel;
	}
}
