﻿using UnityEngine;
using System.Collections;

public class playerLaserSpawnPosition : MonoBehaviour {

	public Vector3 vec_rightLaserSpawn;
	public Vector3 vec_leftLaserSpawn;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		isGunFacingRight();
	}
	
	void isGunFacingRight()
	{
		if (GetComponentInParent<rotatingGun>() != null)
		{
			if (GetComponentInParent<rotatingGun>().b_isGunRight)
			{
				gameObject.transform.localPosition = vec_rightLaserSpawn;
			} 
			else
			{
				gameObject.transform.localPosition = vec_leftLaserSpawn;
			}
		}
	}
}
