using UnityEngine;
using System.Collections;

public class laserShooting : MonoBehaviour 
{
	public float f_Speed;
	public GameObject rotGun;
	public float DestroyTimer;
	private Vector3 dirOflaz;
	public GameObject enemyOne;
	public float f_playerLaserDamage;
	private moNetOneBoss boss;
	private bossShield bossshield;
	public GameObject bSHD;
	private GameObject GO_fan;
	// Use this for initialization
	void Start () 
	{
		rotGun = GameObject.Find ( "GunCenter" );
		rotatingGun dirOfLaserShot = rotGun.GetComponent<rotatingGun>();
		dirOflaz = dirOfLaserShot.vec_directionOfMouse;
		enemyOne = GameObject.FindGameObjectsWithTag("Enemy")[0];
		bSHD = GameObject.FindGameObjectWithTag("Shield");
		GO_fan = GameObject.Find( "fanHitBox" );
	}
	
	void FixedUpdate () 
	{
		DestroyTimer -= Time.deltaTime;
		GetComponent<Rigidbody>().useGravity = false;
		//calls this value from script controlling gun movement will be upgrade to direction of mouse in later patches
		GetComponent<Rigidbody>().velocity = ( dirOflaz * f_Speed );
		//gameObject.transform.Translate(dirOflaz * f_Speed * Time.deltaTime);
		speedClamp();
	}
	
	void OnCollisionEnter ( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == "Platform" )
		{
			Destroy ( gameObject );
		}
		
		if ( DestroyTimer <= 0 )
		{
			Destroy( gameObject );
		}
		
		if ( collidedObject.gameObject.tag == "Enemy" )
		{
			/*firstEnemyBoss enemyHealth = enemy.GetComponent<firstEnemyBoss>();
			enemyHealth.f_currentHealth -= f_playerLaserDamage;
			Destroy(gameObject);*/
			//boss a script calling a component from the game object enemyone which was called previously from start()
			boss = enemyOne.gameObject.GetComponent<moNetOneBoss>();
			boss.moNetHealthModifier( -f_playerLaserDamage );
			Destroy( gameObject );
		}
		
		if ( collidedObject.gameObject.tag == "destructableKillObject" )
		{
			GO_fan.gameObject.GetComponent<DestroyableObjectHealth>().f_destroyableObjectHealth -= f_playerLaserDamage;
			Destroy( gameObject );
		}
		
	}
	
	private void speedClamp( ) 
	{
		if ( GetComponent<Rigidbody>().velocity.magnitude > f_Speed )
		{
			GetComponent<Rigidbody>().velocity = Vector3.ClampMagnitude( GetComponent<Rigidbody>().velocity, f_Speed );
		}
	}
}
