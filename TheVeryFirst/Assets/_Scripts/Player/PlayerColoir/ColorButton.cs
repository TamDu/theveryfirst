using UnityEngine;
using System.Collections;

public class ColorButton : MonoBehaviour 
{
	public bool b_buttonDown;
	public GameObject Gate;
	// Use this for initialization
	void Start () 
	{
		GetComponent<Renderer>().material.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () 
	{
		moveGate();
	}
	
	void OnCollisionEnter( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == "Player" )
		{
			GetComponent<Renderer>().material.color = Color.green;
			b_buttonDown = true;
		}
	}
	
	void moveGate()
	{
		if ( b_buttonDown )
		{
			Gate.gameObject.GetComponent<Renderer>().enabled = false;
			Gate.gameObject.GetComponent<Collider>().enabled = false;
		}
	}	
}
