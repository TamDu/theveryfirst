﻿using UnityEngine;
using System.Collections;

public class playerLaser : MonoBehaviour {

	public GameObject laserShot;
	public Transform laserSpawn;
	public KeyCode shootKey;
	public float f_iFireRate;
	private float f_fireRate;
	
	void Start()
	{
		f_fireRate = 0;
	}
	
	void Update() 
	{
		firingLaser();
	}
	void firingLaser()
	{
		f_fireRate -= Time.deltaTime;
		if (Input.GetKey (shootKey) && f_fireRate <= 0)
		{
			f_fireRate = f_iFireRate;
			Instantiate(laserShot, laserSpawn.position, laserSpawn.rotation);
		}
	}
}
