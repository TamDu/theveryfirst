﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class Health
{
	public float f_CurrentHealth;
	public float f_HealthCapacity;
}

[System.Serializable]
public class Damage
{
	public string s_nameOfTag;
	public float f_DamageModifier;
	public float f_MinCollisionSpeedForDamage;
}

//GOING/NEED TO SPILT THIS AGAIN
public class PlayerHealthAndDamage : MonoBehaviour 
{
	//i means int, and int means inital 10/10
	public int i_lives;
	public Health health;
	public Slider sl_healthBar;
	public Text txt_DeathCount;
	public bool b_resetPlayerPrefs;

	public Damage[] damage;
	
	private int i_deathCount;
	
	private Vector3 vec_CheckPoint;
	
	private void Start () 
	{
		vec_CheckPoint = gameObject.transform.position;
		i_deathCount = PlayerPrefs.GetInt( "PlayerDeathCount", 0 );
	}
	
	// Update is called once per frame
	private void Update() 
	{
		death();
		changingHealthBar();
		deathCount();
		
		if ( b_resetPlayerPrefs )
		{
			PlayerPrefs.DeleteAll();
			PlayerPrefs.SetInt( "RespawnElement", 0 );	
		}
	}

	private void OnCollisionEnter(Collision Other)
	{
		for ( int i_ce = 0; i_ce < damage.Length; i_ce ++ )
		{
			if ( Other.gameObject.tag == damage[i_ce].s_nameOfTag 
				&& Other.relativeVelocity.magnitude 
					> damage[i_ce].f_MinCollisionSpeedForDamage 
				)
			{
				health.f_CurrentHealth -= Other.relativeVelocity.magnitude * damage[i_ce].f_DamageModifier;
				
			}
		}
	}

	private void OnTriggerEnter(Collider Other)
	{
		if ( Other.gameObject.tag == "CheckPoint" )
		{
			vec_CheckPoint = gameObject.transform.position;
		}
	}

	private void death()
	{
		if ( health.f_CurrentHealth <= 0 )
		{
			PlayerPrefs.SetInt( "PlayerDeathCount", i_deathCount + 1 ); 
			gameRestart();
		}
	}
	
	private void gameRestart()
	{
		Application.LoadLevel( 1 );
	}
	
	private void changingHealthBar()
	{
		sl_healthBar.value = health.f_CurrentHealth;
	}
	
	private void deathCount()
	{
		txt_DeathCount.text = "Death Count: " + i_deathCount.ToString();
	}
}
