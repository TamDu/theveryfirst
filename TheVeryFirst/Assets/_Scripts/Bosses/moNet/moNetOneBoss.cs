using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public struct BossMovementSpeed
{
	public float f_checkPointMovementSpeed;
	public float f_chargeMovementSpeedOne;
	public float f_chargeMovementSpeedTwo;
	public float f_chargeMovementSpeedThree;
	
	public float f_normaChargeMovementSpeed;
	public float f_rageChargeMovementSpeed;
}
[System.Serializable]
public struct BossDamage
{
	public float f_normalDamage;
	public float f_chargeOneDamage;
	public float f_chargeTwoDamage;
	public float f_chargeThreeDamage;
	public float f_rageChargeDamage;
}


[System.Serializable]
public struct BossHealth
{
	public float f_currentHealth;
	public float f_health;
}

[System.Serializable]
public struct BossTriggers
{
	public float f_healthAgroOne;
	public float f_healthAgroTwo;		
	public float f_healthAgroThree;
}


[System.Serializable]
public struct BossBoundaries
{
	public float f_minX;
	public float f_MaxX;
	public float f_minY;
	public float f_MaxY;
	public float f_minZ;
	public float f_MaxZ;
}

public enum BOSS_STATES
{
	moNetIdle,
	moNetCheckPointMove,
	moNetSphereAndShield,
	moNetHomingMissile,
	moNetChargeAtPlayer
}

public class moNetOneBoss : MonoBehaviour 
{
	public Transform bossMoveMiddle;
	public Slider sl_moNetHealthBar;
	public Text txt_winScreen;
	public Button[] resetButtons;
	public BossBoundaries bossBoundaries;
	public BossMovementSpeed bossMovementSpeed;
	public BossDamage bossDamage;
	public BossHealth bossHealth;
	public BossTriggers bossTriggers;

	private GameObject GO_player;
	private Triggers b_checkPoint;
	private PlayerHealthAndDamage playerHealth;
	public bool b_bossActivate;
	public float f_bossMoveSpeed;
	private bool b_bossAgroOne;
	private bool b_bossAgroTwo;
	private bool b_bossAgroThree;
	
	private float f_currRotationDuration;
	private float f_currFireTime;
	
	private int i_random;
	
	public BOSS_STATES enum_bossState;
	
	// Use this for initialization
	private void Start () 
	{
		GO_player = GameObject.FindGameObjectWithTag("Player");
		bossHealth.f_currentHealth = bossHealth.f_health;
		f_bossMoveSpeed = bossMovementSpeed.f_normaChargeMovementSpeed;
	}
	
	// Update is called once per frame
	private void Update () 
	{
		boundaries( bossBoundaries );
		bossDeath();
		bossHealthBar();
		winScreen();
	}
	
	private void FixedUpdate()
	{
		bossActivating ();
		
	}
	
	private void winScreen()
	{
		if (txt_winScreen != null)
		{
			if ( bossHealth.f_currentHealth > 0 )
			{
				txt_winScreen.gameObject.SetActive( false );
			}
			
			if ( bossHealth.f_currentHealth <= 0 )
			{
				txt_winScreen.gameObject.SetActive( true );
				for (int i = 0; i < resetButtons.Length; i++)
				{
					resetButtons[i].gameObject.SetActive(true);
				}
			}
		}
	}
	
	private void bossHealthBar()
	{
		if ( enum_bossState == BOSS_STATES.moNetIdle )
		{
			if (sl_moNetHealthBar != null)
				sl_moNetHealthBar.gameObject.SetActive( false );
		}
		else
		{
			if (sl_moNetHealthBar != null)
			{
				sl_moNetHealthBar.gameObject.SetActive( true );
				sl_moNetHealthBar.value = bossHealth.f_currentHealth;
			}
		}
	}
	
	private void boundaries( BossBoundaries bB )
	{
		GetComponent<Rigidbody>().position = new Vector3
		(
			Mathf.Clamp( transform.position.x, bB.f_minX, bB.f_MaxX ),
			Mathf.Clamp( transform.position.y, bB.f_minY, bB.f_MaxY ),
			Mathf.Clamp( transform.position.z, bB.f_minZ, bB.f_MaxZ )
		);
	}
	
	private void bossActivating()
	{
		if ( b_bossActivate )
		{
			if ( bossHealth.f_currentHealth > bossTriggers.f_healthAgroOne )
			{
				enum_bossState = BOSS_STATES.moNetCheckPointMove;
			//	bossMoves();
			}
			if ( bossHealth.f_currentHealth <= bossTriggers.f_healthAgroOne 
				&& bossHealth.f_currentHealth > bossTriggers.f_healthAgroTwo 
				)
			{
				enum_bossState = BOSS_STATES.moNetSphereAndShield;
				
			//	bossMoves();
			}
			if ( bossHealth.f_currentHealth <= bossTriggers.f_healthAgroTwo
				&& bossHealth.f_currentHealth > bossTriggers.f_healthAgroThree
				)
			{
				enum_bossState = BOSS_STATES.moNetHomingMissile;
			//	bossMoves();
			}
			if ( bossHealth.f_currentHealth <= bossTriggers.f_healthAgroThree )
			{
				enum_bossState = BOSS_STATES.moNetChargeAtPlayer;
			//	bossMoves ();
			}
		}
	}
	
	private void bossMoves()
	{
		checkingAgroLevel();
	/*	if ( enum_bossState == BOSS_STATES.moNetCheckPointMove )
		{
			GetComponent<moNetCheckPointMovement>().b_CheckPointMovementActivate = true;
		}
		if ( enum_bossState == BOSS_STATES.moNetSphereAndShield )
		{
			GetComponent<moNetCheckPointMovement>().b_CheckPointMovementActivate = false;
			GetComponent<SmallFighters>().b_isSmallFightersActivate = true;
		}
		if ( enum_bossState == BOSS_STATES.moNetHomingMissile )
		{
			GetComponent<SmallFighters>().b_isSmallFightersActivate = false;
			GetComponent<moNetHomingLaser>().b_isBossHomingLaserActivate = true;
		}
		if ( enum_bossState = BOSS_STATES.moNetChargeAtPlayer )
		{
			GetComponent<moNetHomingLaser>().b_isBossHomingLaserActivate = false;
			GetComponent<moNetChargeAtPlayer>().b_isChargeAtPLayerActive = true;
		} */

	}
	
	public void moNetHealthModifier( float moNetHealthChange )
	{
		if ( GetComponentInChildren<bossShield>().b_isShieldUp == false )
		{
			bossHealth.f_currentHealth += moNetHealthChange;
		}
		else
		{
			GetComponentInChildren<bossShield>().shieldHealthModifier( moNetHealthChange );
		}
	}
		
	private void checkingAgroLevel()
	{
		if ( b_bossAgroOne )
		{
			f_bossMoveSpeed = bossMovementSpeed.f_chargeMovementSpeedOne;
		}
		else
		{
			f_bossMoveSpeed = bossMovementSpeed.f_normaChargeMovementSpeed;
		}
		if ( b_bossAgroTwo )
		{
			f_bossMoveSpeed = bossMovementSpeed.f_chargeMovementSpeedTwo;
		}
		if ( b_bossAgroThree )
		{
			f_bossMoveSpeed = bossMovementSpeed.f_chargeMovementSpeedThree;
		}
	}
	
	private void OnCollisionEnter( Collision collidedObject )
	{
		if ( collidedObject.gameObject.name == "Player" )
		{
			//note to self/kevin why can i not put these a function and then call on them
			playerHealth = GO_player.GetComponent<PlayerHealthAndDamage>();
			collisionDamage( bossMovementSpeed, bossDamage );
		}
	}
	
	private void collisionDamage( BossMovementSpeed bMS, BossDamage bD )
	{
		if ( f_bossMoveSpeed == bMS.f_normaChargeMovementSpeed )
		{
			playerHealth.health.f_CurrentHealth -= bD.f_normalDamage;
		}
		if ( f_bossMoveSpeed == bMS.f_chargeMovementSpeedOne )
		{
			playerHealth.health.f_CurrentHealth -= bD.f_chargeOneDamage;
		}
		if ( f_bossMoveSpeed == bMS.f_chargeMovementSpeedTwo )
		{
			playerHealth.health.f_CurrentHealth -= bD.f_chargeTwoDamage;
		}
		if ( f_bossMoveSpeed == bMS.f_chargeMovementSpeedThree )
		{
			playerHealth.health.f_CurrentHealth -= bD.f_chargeThreeDamage;
		}
		if ( f_bossMoveSpeed == bMS.f_rageChargeMovementSpeed )
		{
			playerHealth.health.f_CurrentHealth -= bD.f_rageChargeDamage;
		}
	}
	
	private void bossDeath()
	{
		if ( bossHealth.f_currentHealth <= 0 )
		{
			Destroy( gameObject );
		}
	}
	
}
