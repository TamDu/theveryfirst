using UnityEngine;
using System.Collections;



public class moNetChargeAtPlayer : MonoBehaviour 
{
	public string s_playerTag;
	public bool b_isChargeAtPLayerActive;
	private bool b_stunned;
	private bool b_collidedPlayer;
	private bool b_startingRageCharge;
	private Vector3 vec_chargeDirection;
	private float f_chargeAtPlayerMS;
	private GameObject player;
	private float f_currStunTimeForWallCrash;
	public float f_stunTimeForWallCrash;
	public bool b_chargeAtPlayer;
	public bool b_canIchargeAtPlayer;
	public bool b_canIMoveMiddle;
	private float f_minDisOfReset;
	
	private void Start () 
	{
		player = GameObject.FindGameObjectWithTag(s_playerTag);
		b_startingRageCharge = false;
		b_canIMoveMiddle = true;
		b_canIchargeAtPlayer = false;
		b_chargeAtPlayer = false;
		b_isChargeAtPLayerActive = false;	
	}
	
	private void Update () 
	{
		activateChargeAtPlayer();
		isBossStunned();
	}
	
	private void activateChargeAtPlayer()
	{
		if ( GetComponent<moNetOneBoss>().enum_bossState == BOSS_STATES.moNetChargeAtPlayer )
		{
			transform.Rotate( 0, 0, 0, Space.World );
			chargeAtPlayer();
		}
	}
	
	private void chargeAtPlayer()
	{
		moveMiddle();
		preparingCharge();
		charging();
	}
	
	private void moveMiddle()
	{
		if ( b_canIMoveMiddle 
			&& !b_stunned
			)
		{
			GetComponent<Renderer>().material.color = Color.yellow;
			f_chargeAtPlayerMS = GetComponent<moNetOneBoss>().bossMovementSpeed.f_normaChargeMovementSpeed;
			//i_currentCheckPoint = 0;
			transform.position = 
			Vector3.MoveTowards
			(	transform.position
			 ,  GetComponent<moNetOneBoss>().bossMoveMiddle.position 
			 ,  f_chargeAtPlayerMS * Time.deltaTime
			); 
			
		}
	}
	
	private void preparingCharge()
	{
		f_minDisOfReset = Vector3.Distance( transform.position, GetComponent<moNetOneBoss>().bossMoveMiddle.position );
		if ( f_minDisOfReset <= 2 )
		{
			b_canIMoveMiddle = false;
			GetComponent<Renderer>().material.color = Color.red;
			vec_chargeDirection = ( player.transform.position - gameObject.transform.position );
			f_chargeAtPlayerMS = GetComponent<moNetOneBoss>().bossMovementSpeed.f_rageChargeMovementSpeed;
			b_canIchargeAtPlayer = true;
		}
	}
	
	private void charging()
	{
		if ( f_chargeAtPlayerMS == GetComponent<moNetOneBoss>().bossMovementSpeed.f_rageChargeMovementSpeed 
			&& b_canIchargeAtPlayer
			)
		{
			GetComponent<Rigidbody>().AddForce( vec_chargeDirection * f_chargeAtPlayerMS, ForceMode.Impulse );
		}
	}
	
	private void isBossStunned()
	{
		if ( b_stunned )
		{
			f_currStunTimeForWallCrash -= Time.deltaTime;
		}
		if ( f_currStunTimeForWallCrash <= 0 )
		{
			b_stunned = false;
			b_canIMoveMiddle = true;
			f_currStunTimeForWallCrash = f_stunTimeForWallCrash;
		}
	}
	
	private void OnCollisionEnter(Collision collidedObject)
	{
		if ( collidedObject.gameObject.name == s_playerTag )
		{
			b_collidedPlayer = true;
			b_canIMoveMiddle = true;
			b_canIchargeAtPlayer = false;
		}
		if ( collidedObject.gameObject.tag == "Platform" )
		{
			b_stunned = true;
			b_canIchargeAtPlayer = false;
			GetComponent<Renderer>().material.color = Color.clear;
		}
	}
}
