using UnityEngine;
using System.Collections;

public class moNetHomingMissile : MonoBehaviour 
{
	private GameObject player;
	public PlayerHealthAndDamage playerHealth;
	public float f_homingLaserDmaage;
	public float f_homingLaserMovementSpeed;
	private Vector3 vec_direction;
	public float f_timeOfDestruction;
	
	private void Start () 
	{
		player = GameObject.FindGameObjectsWithTag("Player")[0];
	}
	
	private void Update () 
	{
		homing ();
		destructionTime();
	}
	
	private void homing()
	{
		vec_direction =	player.transform.position - transform.position;
		GetComponent<Rigidbody>().velocity = vec_direction * f_homingLaserMovementSpeed;
	}
	
	private void OnCollisionEnter( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == "Player" )
		{
			playerHealth = player.GetComponent<PlayerHealthAndDamage>();
			playerHealth.health.f_CurrentHealth -= f_homingLaserDmaage;
			Destroy(gameObject);
		}
		
		if ( collidedObject.gameObject.tag == "Platform" )
		{
			Destroy(gameObject);
		}
	}
	
	private void destructionTime()
	{	
		f_timeOfDestruction -= Time.deltaTime;
		
		if ( f_timeOfDestruction <= 0 )
		{
			Destroy( gameObject );
		}
	}
}
