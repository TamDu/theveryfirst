using UnityEngine;
using System.Collections;


public class moNetActivatingHomingMissile : MonoBehaviour 
{
	private float f_currRotationDurrationOfBeam;
	public float f_durationOfBeam;
	
	public float f_fireTime;
	
	private float f_currRotationChargeDuration;
	private float f_currRotationFireDuration;
	
	
	public GameObject GO_bossMissileLaserBeam;
	public Transform[] bossHomingMissileLaserSpawn;
	public float f_fireRate;
	private float f_timeTillNextShot;
	
	public float f_moNetFireAngle;
	public float f_moNetChargeUpAngle;
	
	public float f_rotationDuration;
	private float f_currRotationDurration;
	
	public float f_resetTime;
	private float f_currResetTime;
	
	private void Start () 
	{
		f_timeTillNextShot = 0;
		f_currRotationDurration = f_rotationDuration;
		f_currResetTime = f_resetTime;
		f_currRotationDurrationOfBeam = f_durationOfBeam;
	}
	
	private void FixedUpdate()
	{
		activationHomingLaserBeam();
	}
	
	private void activationHomingLaserBeam()
	{
		if ( GetComponent<moNetOneBoss>().enum_bossState == BOSS_STATES.moNetHomingMissile )
		{
			homingLaserBeam();
			durationOfFire();
		}
	}
	
	private void homingLaserBeam()
	{	
		transform.position = 
		Vector3.MoveTowards
		(	transform.position
		 ,  GetComponentInParent<moNetOneBoss>().bossMoveMiddle.position
		 ,  GetComponentInParent<moNetOneBoss>().f_bossMoveSpeed * Time.deltaTime
		);			
	}
	
	
	//rotation of boss
	private void firingRotation()
	{
		transform.Rotate( 0, 0, f_moNetFireAngle * Time.deltaTime, Space.World );
		gameObject.GetComponent<Renderer>().material.color = Color.gray;
	}
	
	private void chargeRotation()
	{
		transform.Rotate( 0, 0, -f_moNetChargeUpAngle * Time.deltaTime, Space.World );
		gameObject.GetComponent<Renderer>().material.color = Color.magenta;
	}
	
	
	//fire rate
	private void durationOfFire()
	{
		f_currRotationDurration -= Time.deltaTime;
		
		if ( f_currRotationDurration > 0 )
		{
			spawningLaser();
			firingRotation();
			f_currResetTime = f_resetTime;
		}	
		else
		{
			chargeRotation();
			f_currResetTime -= Time.deltaTime;
		}
		
		if ( f_currResetTime <= 0 )
		{
			f_currRotationDurration = f_rotationDuration;
		}
	}
		
	private void spawningLaser()
	{
		f_timeTillNextShot -= Time.deltaTime;
		
		if ( f_timeTillNextShot <= 0 )
		{
			f_timeTillNextShot = f_fireRate;
			
			for ( int i = 0; i < bossHomingMissileLaserSpawn.Length; i ++ )
			{
				Instantiate
				(	GO_bossMissileLaserBeam
				 ,  bossHomingMissileLaserSpawn[i].position
				 ,  bossHomingMissileLaserSpawn[i].rotation
				);
			}
		}
	}
}
