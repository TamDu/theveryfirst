﻿using UnityEngine;
using System.Collections;

public class movingShipScript : MonoBehaviour 
{
	//This script is for the mini moving ships
	
	public Transform[] checkPoints;		//Array 
	private int currentCheckPoint;		//Current check point 
	public float moveSpeed;				//Move speed of the object between checkpoints
	

	private void Start () 
	{
		transform.position = checkPoints[0].position;
		currentCheckPoint = 0;
	}
	
	// Update is called once per frame
	private void Update () 
	{
		moveBetweenCheckPoints();	//Call the function
	}
	
	
	//Function, to move between points
	private void moveBetweenCheckPoints()
	{
		if(transform.position == checkPoints[currentCheckPoint].position)
		{
			currentCheckPoint ++;
		}
		if(currentCheckPoint >= checkPoints.Length)
		{
			currentCheckPoint = 0;
		}
		
		transform.position = 
		Vector3.MoveTowards
		(	transform.position
		 ,  checkPoints[currentCheckPoint].position
		 ,  moveSpeed * Time.deltaTime
		); 
	}
}