﻿using UnityEngine;
using System.Collections;

public class firingEnemyLaser : MonoBehaviour {


	public GameObject enemyOneLaserShot;
	public Transform[] arr_enemyOneLaserSpawn;
	private bool b_canSphereFire; 
	public float f_iEFireRate;
	private float f_eFireRate;
	private int i_currentElement;
	
	private void Start()
	{
		f_eFireRate = 0;
		b_canSphereFire = false;
	}
	
	private void Update() 
	{
		canSphereFire();
		firingLaser();
	}
	
	private void firingLaser()
	{
		f_eFireRate -= Time.deltaTime;
		//calls bool from parent
		if ( b_canSphereFire 
			&& f_eFireRate <= 0 
			)
		{
			for( i_currentElement = 0; i_currentElement < arr_enemyOneLaserSpawn.Length; i_currentElement ++ )
			{
				Instantiate
				(	enemyOneLaserShot
				 ,  arr_enemyOneLaserSpawn[i_currentElement].position
				 ,  arr_enemyOneLaserSpawn[i_currentElement].rotation
				);
				
				f_eFireRate = f_iEFireRate;
			}
		}
	}
	
	private void canSphereFire()
	{
		if ( GetComponentInParent<SmallFighters>().b_CanSphereFire )
		{
			b_canSphereFire = true;
		}
		else
		{
			b_canSphereFire = false;
		}
	}
}
