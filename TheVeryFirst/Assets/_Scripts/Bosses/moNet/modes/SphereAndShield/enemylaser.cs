using UnityEngine;
using System.Collections;

public class enemylaser : MonoBehaviour {

	private Vector3 direction;
	public float f_speed;
	public float f_enemySphereDamage;
	[HideInInspector]
	public GameObject player;
	public float f_destroyTime;
	private PlayerHealthAndDamage playerHealth;
	private Quaternion rotation;
	
	private void Start () 
	{
		//find inital position of player
		player = GameObject.FindGameObjectsWithTag("Player")[0];
		Vector3 relativePos = player.transform.position - transform.position;
		rotation = Quaternion.LookRotation( relativePos );
		directionOfLaser();
	}

	private void Update () 
	{
		//gameObject.transform.Translate( direction * f_speed * Time.deltaTime );
		GetComponent<Rigidbody>().velocity = direction * f_speed; 
		f_destroyTime -= Time.deltaTime;
		if ( f_destroyTime <= 0 )
		{
			Destroy( gameObject );
		}   
		transform.rotation = Quaternion.Euler( 0f, 0f, rotation.z );
	}
	
	private void directionOfLaser()
	{
		//determines direction of player the moment the enemy laser is created
		direction = player.transform.position - gameObject.transform.position;  
	}
	
	private void OnCollisionEnter( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == "Player" )
		{
			//gets compnent of the player that deals with health and removes a speficied amount
			playerHealth = player.GetComponent<PlayerHealthAndDamage>();
			playerHealth.health.f_CurrentHealth -= f_enemySphereDamage;
			Destroy(gameObject);
		}
		if ( collidedObject.gameObject.tag == "Platform" )
		{
			Destroy( gameObject );
		}
	}
}
