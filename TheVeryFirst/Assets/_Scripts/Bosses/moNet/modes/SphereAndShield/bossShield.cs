﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class bossShield : MonoBehaviour 
{
	public float f_sheildHp;
	public bool b_isShieldUp;
	public Slider sl_shield;
	
	private void Update() 
	{
		checkShieldHp();
		if (sl_shield != null)
			sl_shield.value = f_sheildHp;
	}
	
	private void shieldStatus( bool yesOrNo )
	{
		gameObject.GetComponent<Collider>().enabled = yesOrNo;
		gameObject.GetComponent<Renderer>().enabled = yesOrNo;
		b_isShieldUp = yesOrNo;
	}
	
	private void checkShieldHp()
	{
		if ( f_sheildHp <= 0 )
		{
			 shieldStatus( false );
			 if (sl_shield != null)
			 	sl_shield.gameObject.SetActive( false );
		}
		
		if  ( f_sheildHp > 0 )
		{
			shieldStatus( true );
			if (sl_shield != null)
				sl_shield.gameObject.SetActive( true );
		}
	}
	
	public void shieldHealthModifier( float modifier )
	{
		f_sheildHp += modifier;
	}
}