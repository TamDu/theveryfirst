﻿using UnityEngine;
using System.Collections;

public class SphereRoam : MonoBehaviour {


	[HideInInspector]
	public bool b_canSphereRoam;
	public Transform[] arr_roamingCheckPoints;
	[HideInInspector]
	public float f_sphereRoamMS;
	private int i_currentCheckPoint;
	private Vector3 vec_originalPos;

	private void Start () 
	{
		b_canSphereRoam = false;
		vec_originalPos = transform.localPosition;
	}
	
	private void Update () 
	{
		ifSphereCanRoam();
	}
	
	private void ifSphereCanRoam()
	{
		if ( b_canSphereRoam )
		{
			sphereRoam();
		}
		else
		{
			transform.localPosition = 
			Vector3.MoveTowards
			(	transform.localPosition
			 ,  vec_originalPos
			 ,  f_sphereRoamMS * Time.deltaTime
			);
		}
	}
	
	private void sphereRoam()
	{
		if ( transform.position == arr_roamingCheckPoints[i_currentCheckPoint].position )
		{
			i_currentCheckPoint ++;
		}
		
		if (i_currentCheckPoint == arr_roamingCheckPoints.Length)
		{
			i_currentCheckPoint = 0;
		}
		
		transform.position = 
		Vector3.MoveTowards
		(	transform.position
		 ,  arr_roamingCheckPoints[i_currentCheckPoint].position
		 ,  f_sphereRoamMS * Time.deltaTime
		);
	}
}
