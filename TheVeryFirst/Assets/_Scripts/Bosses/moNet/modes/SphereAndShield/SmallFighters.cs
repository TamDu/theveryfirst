using UnityEngine;
using System.Collections;

public class SmallFighters : MonoBehaviour 
{
	public float f_shieldHealth;
	public float f_sphereRoamingMovementSpeed;
	public bool b_CanSphereFire;
	private bool b_turnOnShield;
	private bool b_turnOnSphereRoam;
	private float f_currSphereAndShield; 
	
	private bool b_activateOnce = true;
	
	private void FixedUpdate()
	{
		activateSphereAndShield();
	}
	
	private void activateSphereAndShield()
	{
		if ( GetComponent<moNetOneBoss>().enum_bossState == BOSS_STATES.moNetSphereAndShield )
		{
			transform.position = Vector3.MoveTowards
			(	transform.position
			 ,  GetComponent<moNetOneBoss>().bossMoveMiddle.position
			 ,  f_sphereRoamingMovementSpeed * Time.deltaTime
			);
			
			activationStatus( true );
		}
		else
		{
			activationStatus( false );
		}
		
		turningOnSphereFireAndSheild();
	}
	
	private void activationStatus( bool yesOrNo )
	{
		b_turnOnShield = yesOrNo;
		b_turnOnSphereRoam = yesOrNo;
		b_CanSphereFire = yesOrNo;
	}
	
	private void turningOnSphereFireAndSheild()
	{
		if ( b_turnOnShield )
		{
			if ( b_activateOnce )
			{
				GetComponentInChildren<bossShield>().shieldHealthModifier( f_shieldHealth );
				b_activateOnce = false;
			}
		}
		
		if ( b_turnOnSphereRoam )
		{
			GetComponentInChildren<SphereRoam>().f_sphereRoamMS = f_sphereRoamingMovementSpeed;
			GetComponentInChildren<SphereRoam>().b_canSphereRoam = true;
		}
		else
		{
			GetComponentInChildren<SphereRoam>().b_canSphereRoam = false;
		}
	}
}
