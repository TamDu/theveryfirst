using UnityEngine;
using System.Collections;

public class moNetCheckPointMovement : MonoBehaviour 
{
	public Transform[] arr_bossCheckPoint;
	private int i_currentCheckPoint;
	
	public float f_bossCheckPointMovementSpeed;
	private float f_minDisToReachCheckPoint;
	
	private void FixedUpdate()
	{
		activationBossCheckPointMovement();
	}
	
	private void activationBossCheckPointMovement()
	{
	
		if ( GetComponent<moNetOneBoss>().enum_bossState == BOSS_STATES.moNetCheckPointMove )
		{
			bossCheckPointMove();
		}
	}
	
	private void bossCheckPointMove()
	{
		GetComponent<Renderer>().material.color = Color.green;
		
		f_bossCheckPointMovementSpeed = GetComponent<moNetOneBoss>().bossMovementSpeed.f_checkPointMovementSpeed;
		f_minDisToReachCheckPoint = Vector3.Distance( transform.position, arr_bossCheckPoint[i_currentCheckPoint].position );
		if(f_minDisToReachCheckPoint <= 2)
		{
			i_currentCheckPoint = Random.Range(0, arr_bossCheckPoint.Length);
		}
		
		transform.position = 
		Vector3.MoveTowards
		(	transform.position
		 ,  arr_bossCheckPoint[i_currentCheckPoint].position
		 ,  f_bossCheckPointMovementSpeed * Time.deltaTime
		); 
	}
}
