﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {

	public GameObject g_o_Player;
	public float f_DistanceAwayFromPlayer;
	private Vector3 vec_InitalCameraPosition;
	void Start () 
	{
	// first line to stop the camera shifting into a strange position if player isn't at 0,0,0
		transform.position = new Vector3 (0.0f, 0.0f, -f_DistanceAwayFromPlayer);
		vec_InitalCameraPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = vec_InitalCameraPosition + g_o_Player.transform.position;
	}
}
