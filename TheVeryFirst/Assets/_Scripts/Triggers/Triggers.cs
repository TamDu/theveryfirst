﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct PlayerHazards
{
	public bool b_Trigger;
	public bool b_instantKill;
	
	public bool b_onEnter;
	public bool b_OnStay;
	public bool b_onExit;
	
	public string s_HazardTag;
	public int i_amountOfDamage;
}

public class Triggers : MonoBehaviour 
{
	private void Start()
	{
		
	}

	public PlayerHealthAndDamage playerHealthAndDamage;
	public PlayerHazards[] arr_playerHazards;
	
	void OnTriggerEnter( Collider triggeredObject )
	{
		for ( int i_ceT = 0; i_ceT < arr_playerHazards.Length; i_ceT ++ )
		{
			if ( arr_playerHazards[i_ceT].b_Trigger 
				&& triggeredObject.gameObject.tag == arr_playerHazards[i_ceT].s_HazardTag
				)
			{
				hazardPunishment( i_ceT );
			}
		}
	}
	
	void OnCollisionEnter( Collision collidedObject )
	{
		for ( int i_ceT = 0; i_ceT < arr_playerHazards.Length; i_ceT ++ )
		{
			if ( !arr_playerHazards[i_ceT].b_Trigger 
				&& arr_playerHazards[i_ceT].b_onEnter
			    && collidedObject.gameObject.tag == arr_playerHazards[i_ceT].s_HazardTag
			    )
			{
				hazardPunishment( i_ceT );
			}
		}
	}
	
	private void OnCollisionStay( Collision collidedObejct )
	{
		for ( int i_ceT = 0; i_ceT < arr_playerHazards.Length; i_ceT ++ )
		{
			if ( !arr_playerHazards[i_ceT].b_Trigger 
				&& arr_playerHazards[i_ceT].b_OnStay
			    && collidedObejct.gameObject.tag == arr_playerHazards[i_ceT].s_HazardTag
			    )
			{
				hazardPunishment( i_ceT );
			}
		}
	}
	
	private void OnCollisionExit( Collision collidedObject )
	{
		for ( int i_ceT = 0; i_ceT < arr_playerHazards.Length; i_ceT ++ )
		{
			if ( !arr_playerHazards[i_ceT].b_Trigger 
				&& arr_playerHazards[i_ceT].b_onExit
			    && collidedObject.gameObject.tag == arr_playerHazards[i_ceT].s_HazardTag
			    )
			{
				hazardPunishment( i_ceT );
			}
		}
	}
	
	void hazardPunishment( int i_ceT )
	{
		if ( arr_playerHazards[i_ceT].b_instantKill )
		{
			playerHealthAndDamage.health.f_CurrentHealth = 0;
			Application.LoadLevel( 0 );
		}
		else
		{
			playerHealthAndDamage.health.f_CurrentHealth -= arr_playerHazards[i_ceT].i_amountOfDamage;
		}
	}
}
