﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct ClosingDoors
{
	public GameObject GO_door;
	public float f_durationOfMovement;
	public float f_current0DurationTime;
}

public class SimpleTrigger : MonoBehaviour 
{
	public string s_playerTag;
	public bool b_hasPlayerEntered = false;
	public float f_doorMoveSpeed;
	public Vector3 vec_direction;
	public float f_interveralDuration;
	public float f_currentDurationTime;
	public int i_currentElement;
	public ClosingDoors[] closingDoors;
	
	private bool b_changeIntervals = true;
	
	private void Start()
	{
		
	}
	
	private void OnTriggerEnter( Collider triggeredObject )
	{
		if ( triggeredObject.gameObject.tag == s_playerTag )
		{
			b_hasPlayerEntered = true;
			GetComponentInChildren<ColourChange>().b_playerDetected = true;
		}
	}
	
	private void FixedUpdate()
	{
		if ( b_hasPlayerEntered )
		{
			checkIntervals();
			movingDoor();
		}
	}
	
	private void checkIntervals()
	{
		if ( b_changeIntervals )
		{
			f_currentDurationTime -= Time.deltaTime;
			if ( f_currentDurationTime < 0 )
			{
				closingDoors[i_currentElement].f_current0DurationTime 
				= closingDoors[i_currentElement].f_durationOfMovement;
				currentElementAddition();
				f_currentDurationTime = f_interveralDuration;
			}
		}
	}
	
	private void currentElementAddition()
	{
		if ( i_currentElement >= (closingDoors.Length - 1) )
		{
			if ( closingDoors[i_currentElement - 1].f_current0DurationTime <= 0 )
			{
				b_changeIntervals = false;
				b_hasPlayerEntered = false;
			}
		}
		else
		{
			i_currentElement ++;
		}
	}
	
	private void movingDoor()
	{
		for ( int i_ceT = 0; i_ceT < i_currentElement; i_ceT ++ )
		{
			closingDoors[i_ceT].f_current0DurationTime -= Time.deltaTime;
			if ( closingDoors[i_ceT].f_current0DurationTime > 0 )
			{
				moveDoor( i_ceT, f_doorMoveSpeed, vec_direction );
			}
		}
	}
	
	private void moveDoor( int i_ceT, float ms, Vector3 direction )
	{
		closingDoors[i_ceT].GO_door.transform.Translate( direction * ms * Time.deltaTime );
	}
}
