﻿using UnityEngine;
using System.Collections;

public class EventOfSimpleTrigger : MonoBehaviour 
{
	public SimpleTrigger simpleTrigger;
	public float f_doorAMovementSpeed;
	public Vector3 vec_doorADirection;
	private Vector3 vec_initialPos;
	
	void Start()
	{
		vec_initialPos = gameObject.transform.position;
	}
	
	void canDoorMove()
	{
		if ( simpleTrigger.b_hasPlayerEntered )
		{
			
		}
		else
		{
		}
	}
	
	void moveToInitalPosition()
	{
		if ( transform.position != vec_initialPos )
		{
			
		}
	}
	
	void doorClosing()
	{
		gameObject.transform.Translate( vec_doorADirection * f_doorAMovementSpeed * Time.deltaTime );
	}
}
