﻿using UnityEngine;
using System.Collections;

public class rotatingGun : MonoBehaviour 
{

	public float f_rotationSpeed;
	public float f_distanceAwayFromCamera;
	[HideInInspector]
	public Vector3 vec_directionOfMouse;
	public bool b_isGunRight;
	//public DrawToMouse getMousePosition;
	//public Transform player;
	//public Vector3 directionOfGun;
	// Use this for initialization
	
	
	//couldn't figure out mouse movement gave up
	void Start () 
	{
	}
	
	void Update()
	{
		checkMousePosition();
		checkLaserSpawn();
	//	rotationRestriction();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
	//	buttonPressCheck();
	//	gameObject.transform.rotation = rotation;
	}
	
	void checkMousePosition()
	{
		Vector3 mousePosition =  Camera.main.ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, f_distanceAwayFromCamera ) );
		vec_directionOfMouse = mousePosition - transform.position;
		Quaternion rotation = Quaternion.LookRotation( vec_directionOfMouse * f_rotationSpeed * Time.deltaTime );
		transform.rotation = rotation;
	}
	
	void checkLaserSpawn()
	{
		if(transform.rotation.eulerAngles.y > 250 && transform.rotation.eulerAngles.y < 280)
		{
			b_isGunRight = false;
		}
		if (transform.rotation.eulerAngles.y > 80 && transform.rotation.eulerAngles.y < 100)
		{
			b_isGunRight = true;
		}
	}
	/*void rotationRestriction()
	{
		Vector3 rotationOfGun = transform.eulerAngles;
		rotationOfGun.z =  Mathf.Clamp(rotationOfGun.z, 0, 0);
		rotationOfGun.y = Mathf.Clamp(rotationOfGun.y, 90,90);
	}*/
	
}

	