﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundaries
{
	public float f_minX;
	public float f_maxX;
	public float f_minY;
	public float f_maxY;
	public float f_minZ;
	public float f_maxZ;
}

public class Restrictions : MonoBehaviour 
{
	public bool activateGravity;
	public bool activateBoundaries;
	public bool activateMaxVelocity;
	
	public float f_MaxVelocity;
	public float f_Gravity;
	public Boundaries boundaries;
	
	void Start () 
	{
		
	}
	
	void Update () 
	{
		checkActivation();
	}
	void FixedUpdate()
	{
		
	}
	
	void checkActivation()
	{
		if ( activateGravity )
		{
			gravityForceDown();
		}
		
		if ( activateBoundaries )
		{
			restrictions( boundaries );
		}
		
		if ( activateMaxVelocity )
		{
			magnitudeRestriction();
		}
	}
	
	void magnitudeRestriction()
	{
		if ( GetComponent<Rigidbody>().velocity.magnitude > f_MaxVelocity )
		{
			GetComponent<Rigidbody>().velocity = Vector3.ClampMagnitude( GetComponent<Rigidbody>().velocity, f_MaxVelocity );
		}
	}
	
	void gravityForceDown() 
	{
		Physics.gravity = new Vector3( 0, -f_Gravity, 0 );
	}
	
	void restrictions( Boundaries res )
	{
		GetComponent<Rigidbody>().position  = new Vector3 
			(	(Mathf.Clamp(GetComponent<Rigidbody>().position.x, res.f_minX, res.f_maxX))
			  , (Mathf.Clamp(GetComponent<Rigidbody>().position.y, res.f_minY, res.f_maxY))
			  , (Mathf.Clamp(GetComponent<Rigidbody>().position.z, res.f_minZ, res.f_maxZ))
			);
	}
}
