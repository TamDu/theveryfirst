﻿using UnityEngine;
using System.Collections;

public class DestroyableObjectHealth : MonoBehaviour 
{
	public float f_destroyableObjectHealth;
	public GameObject[] GO_arr_deleteGameObject;
	
	private void Update()
	{
		checkHealth();
	}
	
	private void checkHealth()
	{
		if ( f_destroyableObjectHealth <= 0 )
		{
			Destroy( gameObject );
			
			for ( int i_ce = 0; i_ce < GO_arr_deleteGameObject.Length; i_ce ++ )
			{
				Destroy( GO_arr_deleteGameObject[i_ce] );
			}
		}
	}
	
	public void objectHealthModifier( float healthModifer )
	{
		f_destroyableObjectHealth += healthModifer;
	}
}
