﻿using UnityEngine;
using System.Collections;

public class TurretGunShot : MonoBehaviour 
{
	public float f_turretDamage;
	private Vector3 vec_directionOfLaser;
	public float f_destroyTimer;
	public float f_laserSpeed;
	public GameObject GO_rotatingTurretGun;
	
	private void Start()
	{
		GO_rotatingTurretGun = GameObject.Find( "TurretCenter" );
		
		rotateTurretGun directionOfLazer = GO_rotatingTurretGun.GetComponent<rotateTurretGun>();
		vec_directionOfLaser = directionOfLazer.vec_directionOfPlayer;
	}
	
	private void Update()
	{
		GetComponent<Rigidbody>().velocity = ( vec_directionOfLaser * f_laserSpeed );
		GetComponent<Rigidbody>().useGravity = false;
		speedClamp();
		destroyTimer();
	}
	
	private void destroyTimer()
	{
		f_destroyTimer -= Time.deltaTime;
		
		if ( f_destroyTimer <= 0 )
		{
			Destroy( gameObject );
		}
	}

	private void OnCollisionEnter ( Collision collidedObject )
	{
		if ( collidedObject.gameObject.tag == "Platform" )
		{
			Destroy ( gameObject );
		}
		
		if ( collidedObject.gameObject.tag == "Player" )
		{
			collidedObject.gameObject.GetComponent<PlayerHealthAndDamage>().health.f_CurrentHealth -= f_turretDamage;
			Destroy( gameObject );
		}
		
		if ( collidedObject.gameObject.tag == "DestroyableObject" )
		{
			collidedObject.gameObject.GetComponent<DestroyableObjectHealth>().objectHealthModifier( -f_turretDamage );
			Destroy( gameObject );	
		}
	}
	
	private void speedClamp( ) 
	{
		if ( GetComponent<Rigidbody>().velocity.magnitude > f_laserSpeed )
		{
			GetComponent<Rigidbody>().velocity = Vector3.ClampMagnitude( GetComponent<Rigidbody>().velocity, f_laserSpeed );
		}
	}
}
