﻿using UnityEngine;
using System.Collections;

public class rotateTurretGun : MonoBehaviour 
{
	public GameObject GO_player;
	public float f_rotationSpeed;
	public Vector3 vec_directionOfPlayer;
	
	public void Update()
	{
		rotateTurret();
	}
	
	private void rotateTurret()
	{
		vec_directionOfPlayer = GO_player.transform.position - transform.position;
		Quaternion rotation = Quaternion.LookRotation( vec_directionOfPlayer * f_rotationSpeed * Time.deltaTime );
		transform.rotation = rotation;
	}	
}
